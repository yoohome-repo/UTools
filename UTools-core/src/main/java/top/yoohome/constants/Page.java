package top.yoohome.constants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 分页信息
 *
 * @author Johnny Zhang
 */
public class Page<T> implements Serializable {
    /** 当前页码 */
    private int pageNo;
    /** 每页记录数 */
    private int pageSize;
    /** 当前页的数量 */
    private int size;
    /** 总记录数 */
    private int total;
    /** 总页数 */
    private int pages;
    /** 起始位置 */
    private int start;
    /** 结束位置 */
    private int end;
    /** 是否第一页 */
    private boolean ifFirstPage;
    /** 是否最后一页 */
    private boolean ifLastPage;
    /** 分页返回的数据 */
    private List<T> dataList;

    public Page() {
        this.pageNo = 1;
        this.pageSize = 10;
        this.size = 0;
        this.start = 0;
        this.end = 9;
        this.total = 0;
        this.pages = 1;
        this.ifFirstPage = false;
        this.ifLastPage = false;
        this.dataList = null;
    }

    public Page(int pageSize, int pageNo) {
        if (pageNo <= 1) {
            pageNo = 1;
        }
        if (pageSize <= 0) {
            pageSize = 10;
        }
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        this.size = 0;
        this.start = 0;
        this.end = 9;
        this.total = 0;
        this.pages = 1;
        this.ifFirstPage = false;
        this.ifLastPage = false;
        this.dataList = null;
    }

    public Page(int pageSize, int pageNo, int total) {
        if (pageNo <= 1) {
            pageNo = 1;
        }
        if (pageSize <= 0) {
            pageSize = 10;
        }
        if (total <= 0) {
            total = 0;
        }
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        this.size = 0;
        this.total = total;
        this.start = (pageNo - 1) * pageSize;
        this.end = this.start + pageSize - 1;
        this.pages = (total - 1) / pageSize + 1;
        this.ifFirstPage = pageNo == 1;
        this.ifLastPage = pageNo * pageSize >= total;
        this.dataList = null;
    }

    public Page(int pageSize, int pageNo, int size, int total) {
        if (pageNo <= 1) {
            pageNo = 1;
        }
        if (pageSize <= 0) {
            pageSize = 10;
        }
        if (size < 1) {
            size = 0;
        }
        if (total <= 0) {
            total = 0;
        }
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        this.size = size;
        this.total = total;
        this.start = (pageNo - 1) * pageSize;
        this.end = this.start + pageSize - 1;
        this.pages = (total - 1) / pageSize + 1;
        this.ifFirstPage = pageNo == 1;
        this.ifLastPage = pageNo * pageSize >= total;
        this.dataList = null;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public boolean isIfFirstPage() {
        return ifFirstPage;
    }

    public void setIfFirstPage(boolean ifFirstPage) {
        this.ifFirstPage = ifFirstPage;
    }

    public boolean isIfLastPage() {
        return ifLastPage;
    }

    public void setIfLastPage(boolean ifLastPage) {
        this.ifLastPage = ifLastPage;
    }

    public List<T> getDataList() {
        return dataList;
    }

    public void setDataList(List<T> dataList) {
        this.dataList = dataList;
    }

    @Override
    public String toString() {
        return "Page{" +
                "pageNo=" + pageNo +
                ", pageSize=" + pageSize +
                ", size=" + size +
                ", total=" + total +
                ", pages=" + pages +
                ", start=" + start +
                ", end=" + end +
                ", ifFirstPage=" + ifFirstPage +
                ", ifLastPage=" + ifLastPage +
                ", dataList=" + dataList +
                '}';
    }
}
