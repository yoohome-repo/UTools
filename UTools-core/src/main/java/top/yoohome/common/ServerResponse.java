package top.yoohome.common;

import top.yoohome.constants.Page;
import top.yoohome.constants.ResponseCode;

import java.io.Serializable;

/**
 * 高可复用响应服务端工具类
 *
 * @author Johnny Zhang
 */
public class ServerResponse<T> implements Serializable {
    /** 响应码 */
    private String code;
    /** 响应码描述 */
    private String msg;
    /** 响应的数据 */
    private T data;
    /** 分页数据 */
    private Page<T> page;

    public ServerResponse() {
    }

    private ServerResponse(String code, String msg) {
        this.code = code;
        this.msg = msg;
        this.data = null;
        this.page = null;
    }

    private ServerResponse(String code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    private ServerResponse(String code, String msg, Page<T> page) {
        this.code = code;
        this.msg = msg;
        this.page = page;
    }

    /**
     * 成功->默认
     *
     * @return <T> 响应包
     */
    public static <T> ServerResponse<T> createBySuccess() {
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(), ResponseCode.SUCCESS.getMsg());
    }

    /**
     * 成功->自定义消息
     *
     * @param msg 消息
     * @return <T> 响应包
     */
    public static <T> ServerResponse<T> createBySuccessMessage(String msg) {
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(), msg);
    }

    /**
     * 成功->自定义数据
     *
     * @param <T> 数据
     * @return <T> 响应包
     */
    public static <T> ServerResponse<T> createBySuccess(T data) {
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(), ResponseCode.SUCCESS.getMsg(), data);
    }

    /**
     * 成功->自定义分页信息
     *
     * @param page 分页数据
     * @return <T> 响应包
     */
    public static <T> ServerResponse<T> createBySuccess(Page<T> page) {
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(), ResponseCode.SUCCESS.getMsg(), page);
    }

    /**
     * 成功->自定义消息和分页信息
     *
     * @param page 分页数据
     * @return <T> 响应包
     */
    public static <T> ServerResponse<T> createBySuccessMessage(String msg, Page<T> page) {
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(), msg, page);
    }

    /**
     * 失败->默认
     *
     * @return <T> 响应包
     */
    public static <T> ServerResponse<T> createByError() {
        return new ServerResponse<T>(ResponseCode.ERROR.getCode(), ResponseCode.ERROR.getMsg());
    }

    /**
     * 失败->自定义消息
     *
     * @param msg 消息
     * @return <T> 响应包
     */
    public static <T> ServerResponse<T> createByErrorMessage(String msg) {
        return new ServerResponse<T>(ResponseCode.ERROR.getCode(), msg);
    }

    /**
     * 失败->自定义数据
     *
     * @param <T> 数据
     * @return <T> 响应包
     */
    public static <T> ServerResponse<T> createByError(T data) {
        return new ServerResponse<T>(ResponseCode.ERROR.getCode(), ResponseCode.ERROR.getMsg(), data);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Page<T> getPage() {
        return page;
    }

    public void setPage(Page<T> page) {
        this.page = page;
    }
}
