package top.yoohome.util;

import java.util.Random;

/**
 * 随机数工具类
 *
 * @author Johnny Zhang
 */
public class RandomUtil {

    /**
     * 生成随机数->字符串
     *
     * @param charCount 生成随机数个数
     * @return String 随机数字符串
     */
    public static String getRandNum(int charCount) {
        StringBuilder charValue = new StringBuilder();
        for (int i = 0; i < charCount; i++) {
            char c = (char) (randomInt(0, 10) + '0');
            charValue.append(c);
        }
        return charValue.toString();
    }

    /**
     * 生成随机数->整型
     *
     * @param from 起点
     * @param to 终点
     * @return int 随机数整型
     */
    private static int randomInt(int from, int to) {
        Random r = new Random();
        return from + r.nextInt(to - from);
    }

}
