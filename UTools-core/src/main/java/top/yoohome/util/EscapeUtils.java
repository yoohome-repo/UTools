package top.yoohome.util;

/**
 * 转义字符工具类
 *
 * @author Johnny Zhang
 */
public class EscapeUtils {

    /** 百分号 */
    private static final String PERCENT_SYMBOL = "%";

    /** 下划线 */
    private static final String UNDERLINE_SYMBOL = "_";

    /** 反斜杠 */
    private static final String BACKSLASH_SYMBOL = "\\";

    /**
     * 百分号，反斜杠，下划线转义
     *
     * @param keyword 需要转移的字符串
     * @return 转义字符串
     */
    public static String transferCharacter(String keyword) {
        if(keyword.contains(PERCENT_SYMBOL) || keyword.contains(UNDERLINE_SYMBOL) || keyword.contains(BACKSLASH_SYMBOL)){
            keyword = keyword.replace("\\", "\\\\")
                    .replace(PERCENT_SYMBOL, "\\%")
                    .replace(UNDERLINE_SYMBOL, "\\_");
        }
        return keyword;
    }
}
