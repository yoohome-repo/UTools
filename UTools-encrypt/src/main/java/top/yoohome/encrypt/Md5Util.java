package top.yoohome.encrypt;

import java.security.MessageDigest;

/**
 * MD5加密工具类
 * @author Johnny Zhang
 */
public class Md5Util {

    private static String passwordSalt = "yoohome37sdkqma23ou89ZXcj@#$@#$#@KJdjklj;D../dSF.,";

    private static final String[] HEXDIGITS = {"0", "1", "2", "3", "4", "5",
            "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};

    /**
     * 获取大写 MD5 字符串，加盐值
     *
     * @param origin byte
     * @return String 字符串
     */
    public static String md5EncodeUtf8(String origin) {
        origin = origin + passwordSalt;
        return md5Encode(origin, "utf-8");
    }

    /**
     * 获取大写 MD5 字符串
     *
     * @param origin 要加密的字符串
     * @param charsetname 编码规则
     * @return String 返回大写MD5
     */
    private static String md5Encode(String origin, String charsetname) {
        String resultString = null;
        try {
            resultString = origin;
            MessageDigest md = MessageDigest.getInstance("MD5");
            if (charsetname == null || "".equals(charsetname)) {
                resultString = byteArrayToHexString(md.digest(resultString.getBytes()));
            }
            else {
                resultString = byteArrayToHexString(md.digest(resultString.getBytes(charsetname)));
            }
        } catch (Exception exception) {
        }
        return resultString.toUpperCase();
    }

    /**
     * 数组转换成string
     *
     * @param b 数组
     * @return String 字符串
     */
    private static String byteArrayToHexString(byte[] b) {
        StringBuffer resultSb = new StringBuffer();
        for (int i = 0; i < b.length; i++) {
            resultSb.append(byteToHexString(b[i]));
        }
        return resultSb.toString();
    }

    /**
     * byte转换成string
     *
     * @param b byte
     * @return String 字符串
     */
    private static String byteToHexString(byte b) {
        int n = b;
        if (n < 0) {
            n += 256;
        }
        int d1 = n / 16;
        int d2 = n % 16;
        return HEXDIGITS[d1] + HEXDIGITS[d2];
    }
}
