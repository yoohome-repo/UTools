package top.yoohome.verify.bean;

import io.jsonwebtoken.impl.DefaultClaims;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.HashMap;

/**
 * Jwt信息存储对象
 *
 * @author Johnny Zhang
 */
public class JwtObject {
    /** 参数集 */
    private DefaultClaims claims;
    /** 用户名 */
    private String username;
    /** 用户编号 */
    private String userId;
    /** 数据集合 */
    HashMap<String, Object> map = new HashMap<>();

    public JwtObject() {
    }

    public JwtObject(String username, String userId, Long expires) {
        if (StringUtils.isBlank(username)) {
            username = "";
        }
        if (StringUtils.isBlank(userId)) {
            userId = "";
        }
        if (expires == null || expires < 1) {
            expires = 60L;
        }
        this.username = username;
        this.userId = userId;
        map.put("username", username);
        map.put("userId",userId);
        long nowMillis = System.currentTimeMillis();
        Date nowTime = new Date(nowMillis);
        long expMillis = nowMillis + expires * 1000;
        Date expTime = new Date(expMillis);
        DefaultClaims claims = new DefaultClaims();
        claims.setIssuedAt(nowTime);
        claims.setExpiration(expTime);
        this.claims = claims;
    }

    public JwtObject(HashMap<String, Object> map, Long expires) {
        if (map.isEmpty()){
            map.put("username", "null");
        }
        if (expires == null || expires < 1) {
            expires = 60L;
        }
        this.map = map;
        long nowMillis = System.currentTimeMillis();
        Date nowTime = new Date(nowMillis);
        long expMillis = nowMillis + expires * 1000;
        Date expTime = new Date(expMillis);
        DefaultClaims claims = new DefaultClaims();
        claims.setIssuedAt(nowTime);
        claims.setExpiration(expTime);
        this.claims = claims;
    }

    public DefaultClaims getClaims() {
        return claims;
    }

    public void setClaims(DefaultClaims claims) {
        this.claims = claims;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public HashMap<String, Object> getMap() {
        return map;
    }

    public void setMap(HashMap<String, Object> map) {
        this.map = map;
    }
}
