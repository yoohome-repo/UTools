package top.yoohome.verify.constants;

/**
 * jwt生成模式枚举类
 *
 * @author Johnny Zhang
 */
public enum JwtModeEnum {
    /** 创建默认jwt，模式 */
    JWT_MODE_DEFAULT(0),

    /** 创建自定义jwt，模式 */
    JWT_MODE_CUSTOM(1);

    private int mode;

    JwtModeEnum(int mode) {
        this.mode = mode;
    }

    public int getMode() {
        return mode;
    }
}
